"use strict";
//1.Тому що клавіатурних подій недостатньо і незручно. Для роботи з input ми застосовуємо спеціальну подію input, яка дозволяє
//вловити зміни в самому input. Розрізняють події change, input, cut, copy, paste.
document.addEventListener("keydown", changeColor);

function changeColor(e) {
    let buttons = document.querySelectorAll('.btn');
    buttons.forEach(button => {
        if (button.dataset.key === e.key.toLowerCase()) {
            button.style.backgroundColor = 'blue';

        } else {
            button.removeAttribute('style');
        }

    })

}

